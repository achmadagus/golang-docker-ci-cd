# Gunakan image Golang sebagai base image
FROM golang:1.20

# Set environment variable
ENV GO111MODULE=on

# Buat direktori untuk aplikasi
WORKDIR /app

# Copy go.mod dan go.sum, dan install dependencies
COPY go.mod ./
# RUN go mod download

# Copy source code ke dalam container
COPY . .

# Build aplikasi
# RUN go build -o main .

# Jalankan aplikasi
# CMD ["./main"]
